#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "vetor.hpp"

class Glider : public Vetor{
  private:

  public:
	   Glider();
     Glider(int linha, int coluna);
     Glider(int linha, int coluna, Vetor *X);
};
#endif
