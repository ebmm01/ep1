#ifndef BLINKER_HPP
#define BLINKER_HPP
#include "vetor.hpp"

class Blinker : public Vetor{
  private:

  public:
	   Blinker();
     Blinker(int linha, int coluna);
     Blinker(int linha, int coluna, Vetor *X);
};
#endif
