#ifndef VETOR_HPP
#define VETOR_HPP

class Vetor{
	private:
	 bool matriz[40][62];
	 bool copia[40][62];
	 int lmax, cmax, coluna, linha;

	public:
    Vetor();
		int getLmax();
		void setLmax(int lmax);

		int getCmax();
		void setCmax(int cmax);

  	void imprimeMatriz();
  	void entradaManual();
		void comparaMatriz();
		void regrasMatriz();
		void conversorFormas(int linha, int coluna);
		void exterminaMatriz();

};

#endif
