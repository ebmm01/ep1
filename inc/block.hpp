#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "vetor.hpp"

class Block : public Vetor{
  private:

  public:
	   Block();
     Block(int linha, int coluna);
     Block(int linha, int coluna, Vetor *X);
};
#endif
