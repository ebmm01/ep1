# Jogo da Vida - EP1

**Aluno: Elias Bernardo Marques Magalhães - 15/0009011**

O Jogo da Vida é um autômato celular elaborado pelo matemático britânico John Horton Conway em 1970.
A célula interage com seus oito vizinhos, que são as células horizontais, verticais ou diagonais.

Só há dois estados possíveis para as células:

>O - Célula viva

>. - Célula morta

Para inicializar o repositório e começar o jogo, crie uma pasta. Dentro dela baixe o repositório e inicie o jogo:

```
cd *pasta criada*
git clone https://gitlab.com/ebmm01/ep1.git
cd ep1
make
make run
```
Maximize a janela do terminal utilizado. O programa tem 7 opções disponíveis:

**[1] Montar o campo manualmente**

>Essa opção permite o usuário montar um campo com uma matriz de tamanho definido por ele.
> Tanto as celulas vivas quanto as coordenadas delas são de escolha do usuário, como também o número de gerações.

**[2] Ver o Block em funcionamento**

>Permite o usuário ver um block com o número de gerações definido por ele.

**[3] Ver o Blinker em funcionamento**

>Permite o usuário ver um Blinker com o número de gerações definido por ele.

**[4] Ver o Glider em funcionamento**

>Permite o usuário ver um Glider com o número de gerações definido por ele.

**[5] Ver a Gosper Glider Gun em funcionamento**

>Permite o usuário ver uma Gosper Glider Gun com o número de gerações definido por ele.

**[6] Ajustar a velocidade de exibição**

>Essa opção permite que o usuário altere a velocidade de atualização do campo.
>A velocidade padrão é de 200.000 microsegundos (0,2 segundos).
>Ao utilizar essa opção, atente-se para o fato de que o número deve ser colocado em microsegundos.

**[7] Encerrar o programa**

>Finaliza o programa.
