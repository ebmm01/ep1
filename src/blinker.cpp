#include "blinker.hpp"

Blinker::Blinker(){
  setLmax(3);
  setCmax(3);
  conversorFormas(1,2);
  conversorFormas(2,2);
  conversorFormas(3,2);

}
Blinker::Blinker(int linha, int coluna, Vetor *X){
  X->conversorFormas(linha, coluna);
  X->conversorFormas(linha+1, coluna);
  X->conversorFormas(linha+2, coluna);
}

Blinker::Blinker(int linha, int coluna){
  conversorFormas(linha, coluna);
  conversorFormas(linha+1, coluna);
  conversorFormas(linha+2, coluna);
}
