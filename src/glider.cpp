#include "glider.hpp"

Glider::Glider(){
  setLmax(10);
  setCmax(10);
  conversorFormas(2,3);
  conversorFormas(3,4);
  conversorFormas(4,2);
  conversorFormas(4,3);
  conversorFormas(4,4);
}
Glider::Glider(int linha, int coluna, Vetor *X){

  X->conversorFormas(linha-1,coluna);
  X->conversorFormas(linha,coluna+1);
  X->conversorFormas(linha+1,coluna-1);
  X->conversorFormas(linha+1,coluna);
  X->conversorFormas(linha,coluna+1);

}
Glider::Glider(int linha, int coluna){
  conversorFormas(linha-1,coluna);
  conversorFormas(linha,coluna+1);
  conversorFormas(linha+1,coluna-1);
  conversorFormas(linha+1,coluna);
  conversorFormas(linha,coluna+1);

}
