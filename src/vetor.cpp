//vetor.cpp
#include<iostream>
#include <stdlib.h>
#include "vetor.hpp"


using namespace std;

Vetor::Vetor(){
	linha = 0;
	coluna = 0;
}

int Vetor::getLmax(){
	return lmax;
}

void Vetor::setLmax(int lmax){
	this->lmax = lmax;
}

int Vetor::getCmax(){
	return cmax;
}

void Vetor::setCmax(int cmax){
	this->cmax = cmax;
}

void Vetor::entradaManual(){
	int nvivos,X,i,j,l,c;
	while(true){
		cout << "\nQual o tamanho da matriz?"<< endl;
		cout << "\nO tamanho deve ser entre 3x3 ~ 40x45 (Não coloque o 'x'!!!):"<< endl;
		cin >> i >> j;
			if (i<3 or i>41 or j<3 or j>45){
				cout << "\nTamanho inválido! Tente novamente.";
				continue;
			}
			else
			setLmax(i), setCmax(j);
			break;
		}

  cout << "\nNúmero de celulas vivas: ";
  cin >> nvivos;
	exterminaMatriz();
  for(X=0;X<nvivos;X++)
  {
      cout << "\nCoordenada da célula " << X+1 << " (Linha 1-40) : ";
      cin >> l;
			cout << "\nCoordenada da célula " << X+1 << " (Coluna 1-44) :";
			cin >> c;
			if (l==0 or l>getLmax() or c==0 or c>getCmax()){
				cout << "\nCoordenada inválida! Tente novamente. \n" << endl;
				X=X-1;
			}
			else{
      matriz[l][c]= true;
			cout << endl;
			system("clear");
      imprimeMatriz();
		}
	}
	system("clear");
	imprimeMatriz();
	cout << "\nEsses são seus organismos vivos. Pressione ENTER para continuar.\n";
	cin.ignore() &&	cin.get();
  cout << endl;
	system("clear");
}

void Vetor::conversorFormas(int linha, int coluna){
	matriz[linha][coluna] = true;
}

void Vetor::imprimeMatriz(){
			for (int linha=0;linha<=(getLmax()+1);linha++){

					for (int coluna=0;coluna<=(getCmax()+1);coluna++){

							if (linha==0 && coluna==0){
								cout<< " --";
							}
							if (linha==getLmax()+1 && coluna==0){
								cout<< " --";
							}
							if (linha==0 && coluna==getCmax()+1){
								cout<< "--";
							}
							if (linha==getLmax()+1 && coluna==getCmax()+1 ){
								cout<< "--";
							}
							if ((linha == 0 or linha == getLmax()+1)&& coluna!=0 && coluna!=getCmax()+1){
								cout << "---";
							}
							if ((coluna == 0 or coluna == getCmax()+1) && linha!=0 && linha!=getLmax()+1){
								cout <<" | ";
							}

							if (matriz[linha][coluna] == true && (linha<=getLmax() && linha>=1 && coluna>=1 && coluna<=getCmax())){
									cout << " O ";
					}
							else if (linha<=getLmax() && linha>=1 && coluna>=1 && coluna<=getCmax())
									cout << " . ";
	} cout << "\n";
	}
}

void Vetor::exterminaMatriz(){
			for (linha=0;linha<=(getLmax()+2);linha++){
					for (coluna=0;coluna<=(getCmax()+2);coluna++){
							matriz[linha][coluna] = false;
	}
	}
}

void Vetor::comparaMatriz(){
    for(linha=0;linha<=(getLmax()+2);linha++){
        for(coluna=0;coluna<=(getCmax()+2);coluna++){

							if(matriz[linha][coluna]== true){
								copia[linha][coluna] = true;
							}
							else
                copia[linha][coluna] = false;
        }
    }
}
void Vetor::regrasMatriz(){
    comparaMatriz();

    for(linha=1; linha<=(getLmax()); linha++){
        for(coluna=1; coluna<=(getCmax()); coluna++){
            int vivos = 0;
            for(int l1 = -1; l1< 2; l1++){

                for(int c1 = -1; c1 < 2; c1++){

                    if(!(l1 == 0 && c1 == 0)) {
                        if(copia[linha+l1][coluna+c1] == true)
				{
					++vivos;
				}
                    }
                }
            }
            if(vivos < 2){
                matriz[linha][coluna] = false;
            }
            else if(vivos == 3){
							matriz[linha][coluna] = true;
            }
            else if(vivos > 3){
                matriz[linha][coluna] = false;
            }
        }
    }
		system("clear");
}
