#include "block.hpp"

Block::Block(){
  setLmax(4);
  setCmax(4);
  conversorFormas(2,2);
  conversorFormas(2,3);
  conversorFormas(3,2);
  conversorFormas(3,3);
}

Block::Block(int linha, int coluna, Vetor *X){
  X->conversorFormas(linha, coluna);
  X->conversorFormas(linha, coluna+1);
  X->conversorFormas(linha+1, coluna);
  X->conversorFormas(linha+1, coluna+1);
}

Block::Block(int linha, int coluna){
  conversorFormas(linha, coluna);
  conversorFormas(linha, coluna+1);
  conversorFormas(linha+1, coluna);
  conversorFormas(linha+1, coluna+1);
}
