//main.cpp
#include <iostream>
#include "vetor.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include <stdlib.h>
#include <unistd.h>

using namespace std;

void Menu()
{
	cout << "\nEscolha uma das opções abaixo:" << endl;
	cout << "\n[1] Montar o campo manualmente" << endl;
	cout << "[2] Ver o Block em funcionamento" << endl;
	cout << "[3] Ver o Blinker em funcionamento" << endl;
	cout << "[4] Ver o Glider em funcionamento" << endl;
	cout << "[5] Ver a Gosper Glider Gun em funcionamento" << endl;
	cout << "[6] Ajustar a velocidade de exibição" << endl;
	cout << "[7] Encerrar o programa\n" << endl;
}

void Intro()
{
	cout << "             O JOGO DA VIDA     "<<endl;
	cout << "\n\n\n\n";
	cout << "O Jogo da Vida é um autômato celular elaborado pelo matemático britânico John Horton Conway em 1970." << endl;
	cout << "A célula interage com seus oito vizinhos, que são as células horizontais, verticais ou diagonais." << endl;
	cout << "\nSó há dois estados possíveis para as células:\n" << endl;
	cout << "O - Célula viva" << endl;
  cout << ". - Célula morta\n" << endl;
	cout << "A velocidade de exibição é de 0,2 segundos por padrão" << endl;
}

int Geracoes(){
	int geracoes;
	cout << "\nQual o número de gerações desejado?\n"<< endl;
	cin >> geracoes;
	return geracoes;
}

void Continuar()
{
	cout << "\nPressione ENTER para continuar.\n";
	cin.ignore() &&	cin.get();
}

int main()
{
	system("clear");
	Intro();
	Menu();
	int i,j;
	char escolha;
	float velocidade = 200000;
	while (escolha!='7'){

	cin>>escolha;

	switch(escolha)
	{
		case '1':{
		Vetor matriz1;

			j=Geracoes();
      matriz1.entradaManual();

			for (i=0; i<j; i++){
				cout<< "Geração:  |" << i << "|" << "\n";
        matriz1.imprimeMatriz();
				usleep(velocidade);
				cout << endl;
       matriz1.regrasMatriz();
		 	}
		Menu();
		matriz1.exterminaMatriz();
	 	break;
    }

		case '2':{
			Block * block = new Block;

				j=Geracoes();
			  block->imprimeMatriz();
				Continuar();

				for (i=0; i<j; i++){
						cout<< "Geração:  |" << i << "|" << "\n";
						block->imprimeMatriz();
						usleep(velocidade);
					  cout << endl;
					  block->regrasMatriz();
				}
			system("clear");
			block->imprimeMatriz();
			Menu();
			block->exterminaMatriz();
			break;
			}

		case '3':{
			Blinker * blinker = new Blinker;

				j=Geracoes();
			  blinker->imprimeMatriz();
				Continuar();

				for (i=0; i<j; i++){
						cout<< "Geração:  |" << i << "|" << "\n";
						blinker->imprimeMatriz();
						usleep(velocidade);
					  cout << endl;
					  blinker->regrasMatriz();
				}
			system("clear");
			blinker->imprimeMatriz();
			Menu();
			blinker->exterminaMatriz();
			break;
			}

			case '4':{
			Glider * glider = new Glider;

				j=Geracoes();
			 	glider->imprimeMatriz();
				Continuar();

				for (i=0; i<j; i++){
						cout<< "Geração:  |" << i << "|" << "\n";
							glider->imprimeMatriz();
							usleep(velocidade);
						  cout << endl;
						  glider->regrasMatriz();
						}
				system("clear");
				glider->imprimeMatriz();
				Menu();
				glider->exterminaMatriz();
				break;
				}

			case '5':
			{
			Vetor gggun;
			gggun.setLmax(28);
		  gggun.setCmax(39);
		  Block b1(6,1, &gggun);
		  Block b2(4,35,&gggun);
		  Blinker b3(6,11,&gggun);
		  Blinker b4(6,17,&gggun);
		  Blinker b5(4,21,&gggun);
		  Blinker b6(4,22,&gggun);
		  gggun.conversorFormas(5,12);
		  gggun.conversorFormas(9,12);
		  gggun.conversorFormas(4,13);
		  gggun.conversorFormas(4,14);
		  gggun.conversorFormas(10,13);
		  gggun.conversorFormas(10,14);
		  gggun.conversorFormas(7,15);
		  gggun.conversorFormas(5,16);
		  gggun.conversorFormas(9,16);
		  gggun.conversorFormas(7,18);
		  gggun.conversorFormas(3,23);
		  gggun.conversorFormas(7,23);
		  gggun.conversorFormas(2,25);
		  gggun.conversorFormas(3,25);
		  gggun.conversorFormas(7,25);
		  gggun.conversorFormas(8,25);

					cout << "\nO número mínimo de gerações recomendado para a Gosper Glider Gun é 70." << endl;
					j=Geracoes();
					gggun.imprimeMatriz();
					Continuar();

					for (i=0; i<j; i++){
							cout<< "Geração:  |" << i << "|" << "\n";
							gggun.imprimeMatriz();
							usleep(velocidade);
							cout << endl;
							gggun.regrasMatriz();
						}
					system("clear");
					gggun.imprimeMatriz();
					Menu();
					gggun.exterminaMatriz();
					break;
			}

			case '6':{
				system("clear");
				cout << "\nA velocidade atual é de "<< velocidade<< " microsegundos\n" <<endl;
				cout<< "1 segundo equivale a 1.000.000 microsegundos."<< endl;
				cout << "\nQual a velocidade desejada? (em microsegundos)\n";
				cin >> velocidade;
				system("clear");
				Menu();
				break;
			}

			case '7':{
				cout << "\nFinalizando..." << endl;
				usleep(1000000);
				break;}

		default:
      cout << "\nOpção inválida\n" << endl;
      Continuar();
			system("clear");
			Menu();
	}
}
	return 0;
}
